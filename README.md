# dcoded.nl website

This repo is the source for [dcoded.nl](https://dcoded.nl) (and arno.gitlab.io).

It is built using [nanoc](https://nanoc.app)

## development

Ensure you have a Procfile runner installed. My favorite is [overmind](https://evilmartians.com/chronicles/introducing-overmind-and-hivemind).

Run `overmind start -f Procfile.dev`. This does a live compile, and hosts your site on [localhost:3000](http://localhost:3000)

## a production build

Ensure your built has `brotli` installed (`apt install -y brotli`). Run these steps:

```
bundle exec nanoc compile --env=prod
find public -type f \( -name "*.js" -o -name "*.html" -o -name "*.css" -o -name "*.pdf" \) -print0 | xargs -0 gzip -9kf
find public -type f \( -name "*.js" -o -name "*.html" -o -name "*.css" -o -name "*.pdf" \) -print0 | xargs -0 brotli --lgwin=22 --best -kf
```

Or, run `bash production-build.sh` from the root of this project

After that, host the resulting `/public` folder on your platform of choice.
