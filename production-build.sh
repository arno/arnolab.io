#!/bin/bash

bundle exec nanoc compile --env=prod
find public -type f \( -name "*.woff" -o -name "*.js" -o -name "*.html" -o -name "*.css" -o -name "*.pdf" \) -print0 | xargs -0 gzip -9kf
find public -type f \( -name "(.woff" -o -name "*.js" -o -name "*.html" -o -name "*.css" -o -name "*.pdf" \) -print0 | xargs -0 brotli --lgwin=22 --best -kf

echo "Remember: host the resulting \`/public\` folder on your platform of choice."
